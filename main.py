import pandas as pd
from Turbine import Turbine
from simulator import generate_random_dataframe

my_turbine = Turbine.load_turbine_config("Vestas_V164") # turbine object can be pickled if necessary

# Incoming data stream
my_turbine.load_sensor_data(data = generate_random_dataframe())
my_turbine.detect_rotor_imbalance()

# New data can come in and turbine object persists
my_turbine.load_sensor_data(data = generate_random_dataframe())
my_turbine.detect_rotor_imbalance()

