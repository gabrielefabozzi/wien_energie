import tomllib
import pandas as pd
import numpy as np
import numpy.typing as npt
from scipy import signal, stats
import logging

logger = logging.getLogger(__name__)

class Turbine:
    ## Turbine class contains all data pertinent to a specific turbine
    ## To be initialized, it will need data to be passed to it
    ## Methods inside the class will be able to retrieve 
    def __init__(self, turbine_name = None, n_blades = None, rpm_min = None, rpm_max = None, ) -> None:
        self.check_inputs(n_blades=n_blades, rpm_min = rpm_min, rpm_max=rpm_max)
        
        self.turbine_name = turbine_name
        self.n_blades = n_blades
        self.rpm_min = rpm_min
        self.rpm_max = rpm_max

        self.sensor_data = None
        self.sensor_config = None
        self.imbalance_ratios = None

    @staticmethod
    def check_inputs(n_blades: int, rpm_min: int, rpm_max: int) -> None:
        """Static method that will contain all necessary checks to be performed for 

        Args:
            n_blades (int): number of blades for the turbine. Must be a positive integer.
            rpm_min (int): minimum operation RPM. Must be a positive integer.
            rpm_max (int): maximum operation RPM. Must be a positive integer greater then rpm_min.
        """
        ## Check if number of blades is 
        if any([n_blades <= 0,
               not isinstance(n_blades, int)]):
            raise ValueError("Number of blades must be a positive integer")
        
        ## Check if minimum and maximum rpm are negative or in the wrong order
        if any([rpm_min <= 0,
                rpm_max <= 0,
               rpm_max < rpm_min]):
            raise ValueError("rpm_max must be higher than rpm_min, and both must be positive")
        
        ## Similar checks would be done on the remaining input
        
    @classmethod
    def load_turbine_config(cls, turbine_name:str, config_file_path:str = "config.toml"):
        """Load turbine configuration. Class method is used as alternative constructor to do appropriate checks

        Args:
            turbine_name (str): name of the turbine to be found in the configuration file
            config_file_path (str, optional): path to a configuration file. Defaults to "config.toml".

        Returns:
            Instantiates a Turbine object from a configuration file
        """
        
        with open(config_file_path, "rb") as f:
            config_data: dict = tomllib.load(f)

            if turbine_name not in config_data:
                raise NameError("There is no configuration for the provided turbine name")

        return cls(turbine_name = turbine_name, **config_data[turbine_name]["turbine_config"])
        
        
    def load_sensor_data(self, data: pd.DataFrame, sensor_config_path:str = "config.toml"):
        """Method to load sensor data to the Turbine class.

        Args:
            data (pd.DataFrame): A pandas DataFrame containing the data
            sensor_config_path (str): relative path to a sensor configuration file. Must contain a "sensor_config" key.
        """
        self.sensor_data = data
        self.sampling_freq = self.sensor_data["ts"].diff().dt.total_seconds().median()
        
        with open(sensor_config_path, "rb") as f:
            config_data: dict = tomllib.load(f)

        try:
            self.sensor_config = config_data[self.turbine_name]["sensor_config"]
        except KeyError as ke:
            print("Please pass a sensor configuration under the 'sensor_config' key.")
        
        
    def detect_rotor_imbalance(self):
        """Method to calculate presence of aerodynamic rotor imbalance
        """
        
        ## Check if nacelle data has been loaded into the Turbine instance
        
        try:
            nacelle_config = self.sensor_config["nacelle_config"]
            nacelle_channels = [nacelle_config["nacelle_sensor"] + str(nacelle_config[channel_direction]) 
                                for channel_direction in ["fore_aft_channel", "side_side_channel", "up_down_channel"]]
        except KeyError as ke:
            print("Make sure the nacelle sensor name and channels are nominated appropriately")
        except TypeError as te:
            print("Make sure the nacelle sensor name is a string")
        
        
        ## Lowpass filter the data. Cutoff frequency is set to the turbine maximum RPM + 4 rotations, to be sure. Normally, this buffer would be a settable parameter.
        
        nacelle_data_filt = self.sensor_data[nacelle_channels].apply(self.low_pass_filter, axis = 0, 
                                                                args = [self.rpm_max + 4])
             
        ## Calculate the RPM with the blade sensor
        
        turbine_rpm = self.rpm_calculator(hertz = True)
        
        ## Calculate the nacelle PSD 
        nacelle_psd = nacelle_data_filt.apply(self.psd_calculator, axis = 0)
        
        imbalance_ratios = {}
        
        for nacelle_channel in nacelle_channels:
        
            ## Find peaks in the nacelle PSD
            
            peak_idx, _ = signal.find_peaks(nacelle_psd[nacelle_channel], prominence = 1000)
            
            ## Find peak frequency in the nacelle PSD closest to 3p and 1p
            
            three_p = min(nacelle_psd.index[peak_idx], key = lambda x: abs(x - turbine_rpm))
            one_p = min(nacelle_psd.index[peak_idx], key = lambda x: abs(x - turbine_rpm/3))

            ## Calculate the 1p/3p imbalance ratio
            
            imbalance_ratios.update({f"{nacelle_channel}_imb_ratio": nacelle_psd.loc[one_p, nacelle_channel] / nacelle_psd.loc[three_p, nacelle_channel]})
        
        self.imbalance_ratios = imbalance_ratios
        

    def rpm_calculator(self, hertz: bool = False) -> float:
        """A function that will calculate the RPM of the blade data passed through the "load_sensor_data" method

        Args:
            hertz (bool, optional): Should the output be in Hertz? If false, returns in RPM. Defaults to False.

        Raises:
            KeyError: in case the data is first not loaded to the class with the "load_sensor_data" method 

        Returns:
            float: the RPM of the Turbine
        """
        
        # Check for blade data presence
        
        try:
            spanwise_sensor = self.sensor_config["blade_config"]["blade_sensors"] + str(self.sensor_config["blade_config"]["spanwise_channel"])
        except KeyError as ke:
            logger.error("Make sure the blade sensor name and channels are nominated appropriately")
            raise
        except TypeError as te:
            logger.error("Make sure the blade sensor name is a string")
            raise
        
        ## Low pass filter the data to remove high-frequency noise
        
        self.sensor_data[f"{spanwise_sensor}_filt"] = self.low_pass_filter(data = self.sensor_data[["a2"]], 
                                                             RPM_cutoff = self.rpm_max + 4)

        
        ## Find the peaks and valleys of the filtered signal
        
        peaks, _ = signal.find_peaks(x = self.sensor_data[[f"{spanwise_sensor}_filt"]].to_numpy().flatten())
        valleys, _ = signal.find_peaks(x = -self.sensor_data[[f"{spanwise_sensor}_filt"]].to_numpy().flatten())
        
        ## If there are no peaks, then rotation is likely to low
        ## In a real situation, this should have been checked by analyzing the acceleration magnitude
        
        if all([len(peaks) == 0, len(valleys) == 0]):
            raise ValueError("Cannot calculate RPM. Rotation is likely too slow")
        
        extremes = np.sort(np.concatenate([peaks, valleys])[1:-1])
        
        ## Calculate RPM and return in appropriate unit
        
        half_rev_rpm = 60/(2*np.diff(extremes)*self.sampling_freq)
        
        half_rev_rpm_non_zero = half_rev_rpm[half_rev_rpm != 0]
        
        if len(half_rev_rpm_non_zero) > 0:
            rpm = stats.hmean(half_rev_rpm_non_zero)
        else:
            rpm = 0
        
        if hertz:
            rpm = rpm / 60
            
        return rpm
        
        
    def low_pass_filter(self, data, RPM_cutoff: float) -> npt.NDArray:
        """A method that will apply a 2nd low-pass digital Butterworth filter with a specific cutoff frequency (expressed in RPM) to a provided signal

        Args:
            data (array-like): an unfiltered signal
            RPM_cutoff (float): cutoff frequency. Expressed in RPM.

        Returns:
            npt.NDArray: filtered signal
        """
        
        b, a = signal.butter(N = 2, Wn = RPM_cutoff/60, fs = np.round(1/self.sampling_freq), btype = "low", analog = False)
        
        return signal.filtfilt(b, a, np.ravel(np.array(data)))
          
    
    def psd_calculator(self, data) -> pd.Series:
        """Method to calculate the power spectral density (PSD) of an array-like object

        Args:
            data array-like: The reaw signal for which to calculate the PSD

        Returns:
            pd.Series: returns a pandas Series object with the frequencies (in Hertz) as index and the PSD as value
        """
        # Taking the real Fourier transform
        ps = np.fft.rfft(data)
        
        # Obtaining the frequencies
        freq = np.fft.rfftfreq(len(data), d = self.sampling_freq)
        
        return pd.Series(np.abs(ps)**2, index=freq)
    
    
    
