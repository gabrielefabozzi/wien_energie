import numpy as np
import datetime
from typing import Tuple
import numpy.typing as npt
import pandas as pd
import ruptures as rpt


def sim_signal(times, RPM: float, sampling_freq: float, minute_length: int = 10, noise_sd: float = 0.05, sig_amp = 1, imb_amp: float = 0.1, phase_offset: float = 0) -> Tuple[npt.NDArray, npt.NDArray]:
    """Simple function to generate periodic signal with noise and an imbalance

    Args:
        RPM (float): rotor RPM
        sampling_freq (float): sampling frequency
        minute_length (int, optional): Interval duration in minutes. Defaults to 10.
        noise_sd (float, optional): scale parameter for the Gaussian noise. Defaults to 0.05.
        imb_amp (float, optional): amplitude for the imbalance component. Defaults to 0.1.
        phase_offset (float, optional): optional phase offset. In radians. Defaults to 0.
        
    Returns:
        Tuple[npt.NDArray, npt.NDArray]: a tuple containing an array of times (in timestamp) and an array of accelerations
    """

    signal = sig_amp*np.sin(2*np.pi*RPM*times/60 + phase_offset/(2*np.pi)*60/RPM)
    imbalance = imb_amp*np.sin((2/3)*np.pi*RPM*times/60 + phase_offset/(2*np.pi)*60/RPM)
    noise = np.random.normal(scale=noise_sd, size = len(times))
    
    return signal + imbalance + noise

def generate_random_dataframe(minute_length: int = 10, sampling_freq: int = 256) -> pd.DataFrame:
    """A function to generate a pandas DataFrame with data looking like acceleration signals from a wind turbine.

    Args:
        minute_length (int, optional): Length of signal in minutes. Defaults to 10.
        sampling_freq (int, optional): Sampling frequency in Hertz. Defaults to 256.

    Returns:
        pd.DataFrame: A pandas DataFrame with a timestamps column and 4 more columns: a2 (blade sensor: spanwise channel), n1 (nacelle sensor: fore-aft), n2 (nacelle sensor: side-side), n3 (nacelle sensor: up-down).
    """
    
    start_time = datetime.datetime(year = 2023, month = 1, day = 11)
    times = np.linspace(start = start_time.timestamp(), stop = start_time.timestamp() + minute_length*60, num = minute_length*60*sampling_freq + 1)[:-1]
    
    # Generating random data for each column
    data = {
        "ts": pd.to_datetime(times, unit = "s"),
        'a2': sim_signal(times = times, RPM = 10, sampling_freq = sampling_freq, imb_amp= 0),
        'n1': sim_signal(times = times, RPM = 12, sampling_freq = sampling_freq, sig_amp=0.1, imb_amp=0.03, noise_sd = 0.01),
        'n2': sim_signal(times = times, RPM = 12, sampling_freq = sampling_freq, sig_amp=0.1, imb_amp=0.03, phase_offset= np.pi/2, noise_sd = 0.01),
        'n3': sim_signal(times = times, RPM = 12, sampling_freq = sampling_freq, sig_amp=0.1, imb_amp=0.03, noise_sd = 0.03)
        # 'wind_speed': np.random.uniform(0, 25, size=rows),
        # 'turbulence_intensity': np.random.uniform(0, 5, size=rows),
        # 'active_power': np.random.uniform(0, 100, size=rows)
    }

    # Creating a DataFrame
    df = pd.DataFrame(data)

    return df


def generate_overtime_ri(n = 500, n_change = 3, sigma = 5):
    
    
    signal, bkps = rpt.pw_linear(n_samples = n, n_features=0, n_bkps=n_change, noise_std=sigma)
    
    return signal, bkps
    
    
    