# Some lines for the plots in the presentation


#%%
import pandas as pd
import plotly.express as px
import numpy as np
from ruptures import Window

## Sample blade data
turbine_data = pd.read_csv("example\data_1.csv")

#%%
fig = px.line(turbine_data, x = "ts", y = ["a1_1", "a1_2", "a1_3"], 
              title = "Sample blade accelerometer data")
fig.update_layout(
    xaxis_title = "Time",
    yaxis_title = "Acceleration (g)",
    legend_title = "Channel"
)
fig.write_html("sample_accelerometer_data.html")
#%%

## Sample PSD

df = turbine_data["a1_1"]
fft = np.fft.rfft(df)
freq = np.fft.rfftfreq(len(df), d = 1/256)

psd = np.abs(fft) / (256 * len(fft))
        
fig = px.line(x = freq, y = psd, log_x = True, title = "Power Spectral Density (a1_1 channel)")
fig.update_layout(
    xaxis_title = "Frequency (Hz)",
    yaxis_title = "Power Spectral Density (g/Hz)",
)
# fig.show()
fig.write_html("psd_a1_1.html")

# pd.Series(np.abs(ps)**2, index=freq)
# %%

## Sample blade data (one channel)
fig = px.line(data_frame = df, x = "ts", y = "a1_2")
fig.update_layout(
    xaxis_title = "Time",
    yaxis_title = "Acceleration (g)",
    title = "Acceleration signal for spanwise channel"
)
fig.write_html("spanwise_channel.html")


#%%
## Simulated overtime measurement
first_signal = np.zeros(500) + 0.05 + np.random.normal(size = 500, scale = 0.01)
second_signal = np.ones(500) + np.random.normal(size = 500, scale = 0.01)


plt.plot(np.arange(0, 1000), np.concatenate([first_signal, second_signal]))
plt.xlabel("Time (10 minute intervals)")
plt.ylabel("1P/3P Ratio")
plt.title("Evolution of rotor imbalance metric over time")